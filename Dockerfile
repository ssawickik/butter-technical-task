FROM python:3.8.1

RUN apt-get update && apt-get install -y \
    netcat \
    postgresql-client \
    postgresql-client-common

ADD . /sources

WORKDIR /sources

RUN pip install -r requirements.txt

EXPOSE 8000

CMD ["./bin/entrypoint.sh"]