#!/bin/sh

echo 'Waiting for database...'
while ! nc -z ${POSTGRES_HOST:-localhost} ${POSTGRES_PORT:-5432}; do echo 'Waiting...'; sleep 5; done
echo 'Database is ready!'
