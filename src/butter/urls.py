from django.conf.urls import url
from django.contrib import admin
from django.urls import include, path
from rest_framework.authtoken import views

from butter.agreements.urls import router as agreements_router


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/api-auth/', include('rest_framework.urls')),
    path('api/v1/api-token-auth/', views.obtain_auth_token),
    url(
        r'^api/v1/agreements/', include(
            (agreements_router.urls, 'agreements'), namespace='api-agreements'
        )
    ),
]
