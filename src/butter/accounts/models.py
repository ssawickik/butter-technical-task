from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    street = models.CharField(max_length=50, blank=True, default='')
    post_code = models.CharField(max_length=50, blank=True, default='')
