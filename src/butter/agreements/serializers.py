from rest_framework import serializers

from .models import AgreementModel


class AgreementSerializer(serializers.ModelSerializer):
    html = serializers.SerializerMethodField('get_html')

    class Meta:
        model = AgreementModel
        fields = ('html', 'user')

    def get_html(self, obj):
        return obj.as_html
