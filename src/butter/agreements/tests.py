from django.contrib.auth import get_user_model
from django.core.management import call_command
from django.urls import reverse
from rest_framework.test import APITestCase


class AgreementAPIViewTestCase(APITestCase):
    url = reverse('api-agreements:agreements-list')

    def setUp(self):
        call_command('loaddata', 'src/butter/demodata.json')

    def test_get_agreement(self):
        user = get_user_model().objects.filter(agreement__isnull=False).first()
        self.client.force_login(user)
        response = self.client.get(self.url)
        self.assertEqual(200, response.status_code)
        self.assertEqual(response.content.decode(), user.agreement.as_html)

    def test_get_agreement_if_agreement_does_not_exist(self):
        user = get_user_model().objects.filter(agreement__isnull=True).first()
        self.client.force_login(user)
        response = self.client.get(self.url)
        self.assertEqual(404, response.status_code)
        self.assertEqual(response.content.decode(), '404 Not Found')

    def test_create_agreement(self):
        user = get_user_model().objects.filter(agreement__isnull=True).first()
        self.client.force_login(user)
        response = self.client.post(self.url)
        self.assertEqual(201, response.status_code)
        self.assertEqual(response.content.decode(), user.agreement.as_html)

    def test_create_agreement_if_agreement_already_exist(self):
        user = get_user_model().objects.filter(agreement__isnull=False).first()
        self.client.force_login(user)
        response = self.client.post(self.url)
        self.assertEqual(400, response.status_code)
        self.assertEqual(response.content.decode(), '400 Bad Request')

# TODO: Add more tests to make sure that everything works fine.
